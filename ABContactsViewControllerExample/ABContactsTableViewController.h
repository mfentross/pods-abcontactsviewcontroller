//
//  ABContactsTableViewController.h
//  ABContactsViewControllerExample
//
//  Created by Malte Fentroß on 05.02.15.
//  Copyright (c) 2015 AppBuddy GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABContactsTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *contacts;

@end
