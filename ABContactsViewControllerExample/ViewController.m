//
//  ViewController.m
//  ABContactsViewControllerExample
//
//  Created by Malte Fentroß on 05.02.15.
//  Copyright (c) 2015 AppBuddy GmbH. All rights reserved.
//

#import "ViewController.h"
#import "ABContactsTableViewController.h"

@interface ViewController () {
    ABContactsTableViewController *_vc;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    _vc = [[ABContactsTableViewController alloc] init];
//    _vc.view.frame = self.view.frame;
//    [self.view addSubview:_vc.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
