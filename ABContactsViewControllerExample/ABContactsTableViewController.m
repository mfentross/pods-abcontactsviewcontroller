//
//  ABContactsTableViewController.m
//  ABContactsViewControllerExample
//
//  Created by Malte Fentroß on 05.02.15.
//  Copyright (c) 2015 AppBuddy GmbH. All rights reserved.
//

#import "ABContactsTableViewController.h"
#import "ContactsData.h"
#import "NBPhoneNumberDefines.h"
#import "NBPhoneNumber.h"
#import "NBPhoneNumberUtil.h"
#import <AddressBook/AddressBook.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface ABContactsTableViewController ()

@end

@implementation ABContactsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _contacts = [NSArray new];
    [self _reloadContacts];

    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(_reloadContacts)
                  forControlEvents:UIControlEventValueChanged];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_contacts count];
}

- (void) _reloadContacts {

    NSArray *contacts = [self _getAllContacts];

    if(contacts != nil) {
        _contacts = contacts;

        [self.tableView reloadData];
    }

    [self.refreshControl endRefreshing];
}

- (NSString *)_parsedPhoneNumber:(NSString *)number {
    NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];

    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];

    NSLog(@"country code is: %@", countryCode);

    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:number
                                 defaultRegion:countryCode error:&anError];


    if (anError == nil) {
        // Should check error
        NSLog(@"isValidPhoneNumber ? [%@]", [phoneUtil isValidNumber:myNumber] ? @"YES":@"NO");

        // E164          : +436766077303
        return [phoneUtil format:myNumber
                    numberFormat:NBEPhoneNumberFormatE164
                           error:&anError];

//        // INTERNATIONAL : +43 676 6077303
//        NSLog(@"INTERNATIONAL : %@", [phoneUtil format:myNumber
//                                          numberFormat:NBEPhoneNumberFormatINTERNATIONAL
//                                                 error:&anError]);
//        // NATIONAL      : 0676 6077303
//        NSLog(@"NATIONAL      : %@", [phoneUtil format:myNumber
//                                          numberFormat:NBEPhoneNumberFormatNATIONAL
//                                                 error:&anError]);
//        // RFC3966       : tel:+43-676-6077303
//        NSLog(@"RFC3966       : %@", [phoneUtil format:myNumber
//                                          numberFormat:NBEPhoneNumberFormatRFC3966
//                                                 error:&anError]);
    } else {

        NSLog(@"fatal error in parsing phone number: %@", anError.description);

        return nil;
    }
}

- (NSArray *)_getAllContacts
{
    
    CFErrorRef *error = nil;
    
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    __block BOOL accessGranted = NO;
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        
#ifdef DEBUG
        NSLog(@"Fetching contact info ----> ");
#endif
        
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
        NSMutableArray* items = [NSMutableArray arrayWithCapacity:nPeople];
        
        
        for (int i = 0; i < nPeople; i++)
        {
            ContactsData *contacts = [ContactsData new];
            
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            //get First Name and Last Name
            
            contacts.firstName = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            
            contacts.lastName =  (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            if (!contacts.firstName) {
                contacts.firstName = @"";
            }
            if (!contacts.lastName) {
                contacts.lastName = @"";
            }
            
            // get contacts picture, if pic doesn't exists, show standart one
            
            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(person);
            contacts.image = [UIImage imageWithData:imgData];
            if (!contacts.image) {
                contacts.image = [UIImage imageNamed:@"NOIMG.png"];
            }
            //get Phone Numbers
            
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
                
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
                [phoneNumbers addObject:[self _parsedPhoneNumber:phoneNumber]];
                NSLog(@"current number: %@ now is: %@", phoneNumber, [self _parsedPhoneNumber:phoneNumber]);
                //NSLog(@"All numbers %@", phoneNumbers);
                
            }
            
            contacts.phoneNumbers = phoneNumbers;
            
            //get Contact email
            
//            NSMutableArray *contactEmails = [NSMutableArray new];
//            ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
//
//            for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
//                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
//                NSString *contactEmail = (__bridge NSString *)contactEmailRef;
//
//                [contactEmails addObject:contactEmail];
//                // NSLog(@"All emails are:%@", contactEmails);
//
//            }
//
//            [contacts setEmails:contactEmails];
//
            
            
            [items addObject:contacts];
            
#ifdef DEBUG
            //NSLog(@"Person is: %@", contacts.firstNames);
            //NSLog(@"Phones are: %@", contacts.numbers);
            //NSLog(@"Email is:%@", contacts.emails);
#endif
            
            
            
            
        }
        
        return items;
        
        
        
    } else {
#ifdef DEBUG
        NSLog(@"Cannot fetch Contacts :( ");        
#endif
        return nil;
        
        
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *simpleTableIdentifier = @"SimpleTableItem";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }

    ContactsData *contact = (ContactsData *) [_contacts objectAtIndex:indexPath.row];

    NSLog(@"contact name: %@ %@", contact.firstName, contact.lastName);

    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
    cell.imageView.image = contact.image;

    cell.detailTextLabel.text = [NSString stringWithFormat:@"%i", indexPath.row];


    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
