//
//  ContactsData.h
//  ABContactsViewControllerExample
//
//  Created by Malte Fentroß on 05.02.15.
//  Copyright (c) 2015 AppBuddy GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ContactsData : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSArray *phoneNumbers;


@end
